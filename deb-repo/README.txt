Hello Kano World!
These package repositories contain some extra packages and tweaks that pierce_jason think could improve the Kano experience.

Usage instructions:

# Add this repository to Debian package management
sudo sh -c "echo deb http://kanojp.bitbucket.org/deb-repo Kanux2.1 tweaks > /etc/apt/sources.list.d/kanojp-tweaks.list";

# Update Debian package lists
sudo apt-get update;

# Show packages available from the kanojp.bitbucket.org repository that you just added
grep "Package: " /var/lib/apt/lists/kanojp.*
